""" challenge 1:
    take the given list of lists of strings and display it in a well organized table with each column right justified
    look for the longest string in each of the inner lists so that the whole column can be wide enough to fit all of
    the strings. store the max width of each column as a list of integers

    challenge 2:
    do the same shit as above, but alphabetize the list of lists of strings first.

    tmp = []
    for row in tableData:
	for column in row:
		tmp.append(column)
	tmp.sort(key=str.lower)

	above code would create a single, alphebetized list. after that, you would need to slice that list up into a list of
	lists of strings
"""


tableData = [['apples', 'oranges', 'cherries', 'banana'],
             ['Alice', 'Bob', 'Carol', 'David'],
             ['dogs', 'cats', 'moose', 'goose']]


def printTable():
    # figures out the longest string in each of the nested lists and stores the lengths in a list lol
    i = 0
    colWidths = [0, 0, 0]

    for list in tableData:
        for item in list:
            if len(item) > colWidths[i]:
                colWidths[i] = len(item)
        i += 1

    i = 0

    """
        bruteforce visualization
        print(tableData[0][0].rjust(colWidths[0]) + ' ' + tableData[1][0].rjust(colWidths[1]) + ' ' + tableData[2][0].rjust(
            colWidths[2]))
        print(tableData[0][1].rjust(colWidths[0]) + ' ' + tableData[1][1].rjust(colWidths[1]) + ' ' + tableData[2][1].rjust(
            colWidths[2]))
        print(tableData[0][2].rjust(colWidths[0]) + ' ' + tableData[1][2].rjust(colWidths[1]) + ' ' + tableData[2][2].rjust(
            colWidths[2]))
        print(tableData[0][3].rjust(colWidths[0]) + ' ' + tableData[1][3].rjust(colWidths[1]) + ' ' + tableData[2][3].rjust(
            colWidths[2]))
    """

    """ the code below is wrong because:
        everything is being printed in a single statement, so k would just end up printing a lot of duplicate data
        each iteration of the loop

    for i in range(len(tableData)):
        for k in range(len(tableData[0])):
            print(tableData[i][k].rjust(colWidths[i]) + ' ' + tableData[i][k].rjust(colWidths[i]) + ' ' + tableData[i][k].rjust(
        colWidths[i]))
    """

    k = 0  # what is a more elegant solution? is there one given the constraints?

    for i in range(len(tableData[0])):
        print(tableData[k][i].rjust(colWidths[k]) + ' ' + tableData[k+1][i].rjust(colWidths[k+1]) + ' ' + tableData[k+2][i].rjust(colWidths[k+2]))

def printAlphaTable():


printTable()
